require 'selenium-webdriver'
require 'rspec'
require 'httparty'
require 'capybara'
require 'site_prism'
require 'capybara/dsl'
require 'faker'
require 'pry'
require "capybara/cucumber"

Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  config.app_host = "http://www.inmrobo.tk/"
end
