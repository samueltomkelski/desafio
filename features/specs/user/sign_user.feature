#encoding: utf-8
#language: pt

Funcionalidade: Cadastro de usuário
Eu como usuário
Quero poder efetuar o cadastro no site
Para poder acessar as funcionalidades disponíveis

@signUp_user
Cenário: Efetuar o cadastro de um usuário válido

Dado que acesse a tela de cadastro de usuário
Quando informar os dados de cadastro e concluir
Então o usuário deverá ser redireciondo para a tela de login