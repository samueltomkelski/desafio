#encoding: utf-8
#language: pt

Funcionalidade: Login
Eu como usuário
Quero poder efetuar o login no site
Para poder acessar as funcionalidades disponíveis

@signin_user
Cenário: Efetuar o login no site

Dado que acesse a tela de login
Quando informar os dados de login e concluir
Então o usuário deverá ser autenticado e logado