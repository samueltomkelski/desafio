#encoding: utf-8
#language: pt

Funcionalidade: Cadastrar funcionário
Eu como usuário do sistema
Quero poder acionar a inclusão de funcionário
Para poder inserir um novo funcionário ao sistema

Contexto: Logado no sistema
* logado no sistema

@new_employee_clt
Cenário: Efetuar cadastro de um funcionário CLT

Dado que acesse o sistema
Quando acionar a opção para inserir um funcionário CLT
E preencher os dados e confirmar
Então a inclusão deverá ser confirmada
