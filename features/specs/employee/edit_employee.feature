#encoding: utf-8
#language: pt

Funcionalidade: Editar funcionário
Eu como usuário do sistema
Quero poder acionar a edição de funcionário
Para poder alterar os dados necessários

Contexto: Logado no sistema
* logado no sistema

@edit_employee
Cenário: Efetuar edição de funcionário

Dado que possua um funcionário para edição
Quando informar acionar a opção para editar o funcionário
E preencher os dados para edição e confirmar
Então a alteração deverá ser confirmada