  Dado('que acesse o sistema') do
    @newemploye = Newemployee.new
    @newemploye.load
  end

  Quando('acionar a opção para inserir um funcionário CLT') do
   @newemploye.link_new_employee.click
  end
  
  Quando('preencher os dados e confirmar') do
    name.set Faker::Name.name
    cpf.set Faker::CPF.numeric 
    gender.select_option 
    admission.set Faker::Date.in_date_period(month: 2) 
    position.set Faker::Construction.trade 
    salary.set Faker::Number.decimal(l_digits: 4, r_digits: 2) 
    type_positon.set true
  
    @newemploye.newemployee(name, cpf, gender, admission, position, salary, type_positon)
  end
  
  Então('a inclusão deverá ser confirmada') do
    expect(page).to have_content 'SUCESSO! Usuário cadastrado com sucesso'
  end