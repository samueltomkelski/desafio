Dado('que possua um funcionário para edição') do
    @editemployee = Editemployee.new
    @editemployee.load
  end

  Quando('informar acionar a opção para editar o funcionário') do
    @editemployee.search('Dirk Kuyt')
  end
  
  Quando('preencher os dados para edição e confirmar') do
    admission.set Faker::Date.in_date_period(month: 2) 
    position.set Faker::Construction.trade 
    salary.set Faker::Number.decimal(l_digits: 4, r_digits: 2) 

    @editemployee.edit_employee(admission, position, salary)
  end
  
  Então('a alteração deverá ser confirmada') do
    expect(page).to have_content 'SUCESSO! Usuário alterado com sucesso'
  end