  Dado(/^logado no sistema$/) do
  @user = SigninPage.new
  @user.load

  usr = 'samueltomkelski'
  password = '123456'
  @user.signin_user(usr, password)
end


Dado('que acesse a tela de login') do
    @user = SigninPage.new
    @user.load
  end
  
  Quando('informar os dados de login e concluir') do
    usr = 'samueltomkelski'
    password = '123456'
  
    @user.signin_user(usr, password)
  end
  
  Então('o usuário deverá ser autenticado e logado') do
    expect(page).to have_content 'Pesquisar'
  end