Dado('que acesse a tela de cadastro de usuário') do
    @user = UserPage.new
    @user.load
  end
  
  Quando('informar os dados de cadastro e concluir') do
    usr = Faker::Internet.username(specifier: 8)
    password = '123456'
    password_verification = '123456'
  
    @user.signUp_user(usr, password, password_verification)
  end
  
  Então('o usuário deverá ser redireciondo para a tela de login') do
    expect(@user.has_current_path?('/accounts/login/')).to eq true
  end