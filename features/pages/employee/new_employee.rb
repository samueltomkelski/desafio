class Newemployee < SitePrism::Page
    set_url'empregados/new_empregado'

    #link
    element :link_new_employee, '#navbarSupportedContent > ul > li:nth-child(2) > a'

    #input
    element :input_name, 'input[id="inputNome"]'
    element :input_cpf, 'input[id="cpf"]'

    element :input_admission, 'input[id="inputAdmissao"]'
    element :input_position, 'input[id="inputCargo"]'
    element :input_salary, 'input[id="dinheiro"]'

    #select
    element :select_gender, 'input[id="slctSexo"]'

    #radio
    element :radio_cpf, 'input[id="clt"]'
    element :radio_pj, 'input[id="pj"]'

    #button
    element :button_new, 'input[class="cadastrar-form-btn"]'

    def newemployee(name, cpf, gender, admission, position, salary, type_positon)
        self.input_name.set name
        self.input_cpf.set cpf
        self.select_gender.set gender
        self.input_admission.set admission
        self.input_position.set position
        self.input_name.set salary
        self.type_positon.set type_positon
    end

end