class EmployeeslistPage < SitePrism::Page
    element :table_list_employees, 'table[id="tabela"]'

    #link
    element :link_new_employee, 'a[class="nav-link text-uppercase font-weight-bold"]', :text => 'Funcionário'
end