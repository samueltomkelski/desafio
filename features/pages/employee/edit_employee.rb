class Editemployee < SitePrism::Page
    set_url'empregados/'
    
    #button
    element :button_edit, 'button[class="btn btn-warning"]', match: :first

    #input
    element :input_search, '#tabela_filter input'
    element :input_admission, 'input[id="inputAdmissao"]'
    element :input_position, 'input[id="inputCargo"]'
    element :input_salary, 'input[id="dinheiro"]'


    def search(search)
        self.input_search.set search
    end


    def edit_employee(admission, position, salary)
        button_edit.click

        self.input_admission.set admission
        self.input_position.set position
        self.input_name.set salary

    end

end