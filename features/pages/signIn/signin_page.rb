class SigninPage < SitePrism::Page
    set_url'accounts/login/'

    #links
    element :link_cadastrar, 'a[class="txt2 bo1"]'

    #inputs
    element :input_user, 'input[name="username"]'
    element :input_password, 'input[name="pass"]'

    #buttons
    element :button_sign, 'button[class="login100-form-btn"]'

    #span
    element :span_login, 'span[class="login100-form-title p-b-1"]', :text => 'Login'


    def signin_user(usr, password)
        self.input_user.set usr
        self.input_password.set password

        button_sign.click
    end 
end