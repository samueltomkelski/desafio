class UserPage < SitePrism::Page
    set_url'accounts/signup/'

    #links
    element :link_cadastrar, 'a[class="txt2 bo1"]'

    #inputs
    element :input_user, 'input[name="username"]'
    element :input_password, 'input[name="pass"]'
    element :input_password_verification, 'input[name="confirmpass"]'

    #buttons
    element :button_sign, 'button[class="login100-form-btn"]'

    #span
    element :span_login, 'span[class="login100-form-title p-b-1"]', :text => 'Login'


    def signUp_user(usr, password, password_verification)
        self.input_user.set usr
        self.input_password.set password
        self.input_password_verification.set password_verification

        button_sign.click
    end 
end